import React, {Component} from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Nav from "./components/nav.component";
import Home from "./components/home.component";
import Login from "./components/login.component";
import Signup from "./components/signup.component";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import axios from 'axios';


export default class App extends Component{
  state={}
  componentDidMount=()=>{
    const config={
        headers:{
       
        }
     }
     axios.get('user',config).then(
         res=>{
        this.setUser(res.data);
          
         },
           err=>{
        console.log(err)
          }
       )
         };
         setUser = user=>{
this.setState=({
  user: user
});
         };

  render()
  {

return(
  <BrowserRouter>
<div className="App">
  <Nav user="this.state.user"/>
  <div className="auth-wrapper">
    <div className="auth-inner">
      <Switch>
        <Route exact path="/" component={()=>< Home user ={this.state.user}/> }/>
        <Route path="/login" component={()=> < Login setUser={this.setUser}/>}/>
        <Route path="/signup" component={Signup}/>
        
      </Switch>
     <Home/>
    </div>
  </div>
</div>
</BrowserRouter>
);
}
}





