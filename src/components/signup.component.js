import axios from 'axios';
import { Component } from 'react';

export default class Signup extends Component {
     handleSubmit=e=>{
        e.preventDefault();
         const data={
           email_phone :this.email,
           dia_chi :this.diachi,
          mat_khau : this.matkhau,
          xac_nhan :this.xacnhan,
         };
         axios.post('signup',data).then(
       res=>{
         console.log(res)
      }
     ).catch(
       err=>{
       console.log(err);
       }
     )
     };
  render(){
  return (
    
      
      <form onSubmit={"this.handleSubmit"}>
          <h1 class="custom_h1">Sign Up MyAppStore</h1>
        <h2 class="form-tittle">Sign Up MyAppStore</h2>
        <div className="form-group">
        <label>Email hoặc Số điện thoại</label>
        <input type="text" className="form-control" placeholder="Email-Phone Đăng kí"
        onChange={e=>this.email=e.target.value} />
        </div>
        <div className="form-group">
        <label>Mật khẩu</label>
        <input type="text" className="form-control" placeholder="Mật khẩu"  onChange={e=>this.matkhau=e.target.value}  />
        </div>
        <div className="form-group">
        <label>Xác nhận mật khẩu</label>
        <input type="text" className="form-control" placeholder="Xác nhận mật khẩu"  onChange={e=>this.xacnhan=e.target.value}  />
        </div>
        <div className="form-group">
        <label>Địa chỉ</label>
        <input type="text" className="form-control" placeholder="Địa chỉ"  onChange={e=>this.diachi=e.target.value}  />
        </div>
        <button className="btn btn-primary btn-block">Đăng Kí</button>
    
   
    </form>
    
    
   
  );
}
}


