import { Component } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';
export default class Login extends Component {
  state={}
    handleSubmit=e=>{
       e.preventDefault();
       const data={
           email_phone :this.email,
          dia_chi :this.diachi,
          mat_khau : this.matkhau,
          xac_nhan :this.xacnhan,
        };
        axios.post('login',data).then(
       res=>{
         localStorage.setItem('token',res.data.token);
         this.setState({
           loggedIn: true
         });
         this.props.setUser(res.data.user);
       }
     ).catch(
       err=>{
       console.log(err);
       }
    )
   };
  render(){
    if(this.state.loggedIn){
      return<Link to={'/'}
      />;
    }
  return (
    
      
      <form onSubmit={"this.handleSubmit"}>
          <h1 className="custom_h1">Login MyAppStore</h1>
        <h2 className="form-tittle">Login MyAppStore</h2>
        <div className="form-group">
        <label>Email hoặc Số điện thoại</label>
        <input type="text" className="form-control" placeholder="Email-Phone Đăng nhập"
        onChange={e=>this.email=e.target.value} />
        </div>
        <div className="form-group">
        <label>Mật khẩu</label>
        <input type="text" className="form-control" placeholder="Mật khẩu"  onChange={e=>this.matkhau=e.target.value}  />
        </div>
        {/* <div className="form-group">
        <label>Xác nhận mật khẩu</label>
        <input type="text" className="form-control" placeholder="Xác nhận mật khẩu"  onChange={e=>this.xacnhan=e.target.value}  />
        </div> */}
        <div className="form-group">
        <label>Địa chỉ</label>
        <input type="text" className="form-control" placeholder="Địa chỉ"  onChange={e=>this.diachi=e.target.value}  />
        </div>
        <button className="btn btn-primary btn-block">Đăng Nhập</button>
        <div class="custom_login">
        <span class="login_with">
          <span> Hoặc đăng nhập với </span> 
        </span>
        <a href="#" class="login_with_facebook"><i class="fab fa-facebook"></i></a> 
        ||
        <a href="#" class="login_with_google"><i class="fab fa-google"></i></a>
      </div>
      <div class="login_question">
       Bạn chưa có tài khoản?,
       <Link to={'/signup'} className="nav-link">Đăng kí ngay</Link>


      </div>
      <div class="myapp_custom">
      <div class="container">
        <div class="row">
        
          <div class="col-sm-4">
     
        <div class="custom_target">
            <a target="_blank" href="https://www.apple.com/legal/sales-support/applecare/docs/techsupportvi.html">Trợ giúp</a>
            </div>
            </div>
            <div class="col-sm-4">
            <div class="custom_target">
            <a target="_blank" href="https://www.apple.com/legal/privacy/vn/">Chính sách bảo mật</a>
            </div>
            </div>
            <div class="col-sm-4">
            <div class="custom_target">
            <a target="_blank" href="https://www.apple.com/legal/internet-services/itunes/vn/terms.html">Điều khoản sử dụng</a>
            </div>
         </div>
    </div>
    </div>
    </div>
    
    
   
    </form>
    
    
   
  );
}
}