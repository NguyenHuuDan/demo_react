import { Component } from "react";
import { Link } from "react-router-dom";
export default class Nav extends Component{
render(){
     let buttons;
     
       if(this.props.user){
        buttons=(
          <ul className=" navbar-nav ml-auto">
         <li className="nav-item">
            <Link to={'/'} onClick={()=>localStorage.clear} className="nav-link">Đăng Xuất</Link>
          </li>
          {/* <li className="nav-item">
          <Link to={'/signup'} className="nav-link">Đăng Kí</Link>
           </li> */}
        </ul>)
         }
         else{
           buttons=(
              <ul className=" navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={'/login'} className="nav-link">Đăng Nhập</Link>
            </li>
             <li className="nav-item">
              <Link to={'/signup'} className="nav-link">Đăng Kí</Link>
            </li> 
          </ul> )
          
    }
    return(
        <nav className="navbar navbar-expand navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand"to={'/'}>Trang Chủ</Link>
        <div className="collapse navbar-collapse">
        {/* <ul className=" navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={'/login'} className="nav-link">Đăng Nhập</Link>
            </li>
             <li className="nav-item">
              <Link to={'/signup'} className="nav-link">Đăng Kí</Link>
            </li> 
          </ul> */}
          {buttons}
        </div>
        </div>
      </nav>
    )
    
}
}

