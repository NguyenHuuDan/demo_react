import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import axios from 'axios';
import reportWebVitals from './reportWebVitals';



axios.defaults.baseURL="http://localhost:3000/";
// axios.defaults.headers.document['Authorization']='Welcom to MyAppStore'+localStorage.getItem('token'); 
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();

